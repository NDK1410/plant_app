import { Image } from "react-native";
import React from "react";
import HomePage from "../screens/HomePage";
import Favourite from "../screens/Favourite";
import Profile from "../screens/Profile";
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

// Bottom Navigator //
const Tab = createBottomTabNavigator();
const BottomTabNavigator = () => {
  return(
    <Tab.Navigator
      tabBarOptions={{
        style:{
          height: 65,
          justifyContent: "center",
          elevation: 2,
          paddingVertical: 15,
          backgroundColor: "eff4f0",
        }
      }}
    >

      <Tab.Screen
        name = "HomePage"
        component={HomePage}
        options={{
          tabBarLaBel:"",
          tabBarIcon:({color, size}) => (
            <Image
              source={require("../../assets/images/8.png")}
              style={{ height:20, width:20 }}
            />
          )
        }}
      />

      <Tab.Screen
        name = "Favourite"
        component={Favourite}
        options={{
          tabBarLaBel:"",
          tabBarIcon:({color, size}) => (
            <Image
              source={require("../../assets/images/9.png")}
              style={{ height:20, width:20 }}
            />
          )
        }}
      />

      <Tab.Screen
        name = "Profile"
        component={Profile}
        options={{
          tabBarLaBel:"",
          tabBarIcon:({color, size}) => (
            <Image
              source={require("../../assets/images/10.png")}
              style={{ height:20, width:20 }}
            />
          )
        }}
      />
    </Tab.Navigator>
  );
};

const Stack = createStackNavigator();
const screenOptionStyle = { headerShown: false }

const HomeStackNavigator = () => {
  return (
    <Stack.Navigator  screenOptions={screenOptionStyle}>
      <Stack.Screen name="Shop" component={BottomTabNavigator} />
    </Stack.Navigator>
  )
}

export default HomeStackNavigator;
