import React from "react";
import { StyleSheet, Text, View, Image, ImageBackground } from 'react-native';
import { LinearGradient } from "expo-linear-gradient";
import { TextInput, ScrollView, TouchableOpacity } from "react-native-gesture-handler";

const HomePage = () => {
    return (
      <View
        style={{
          backgroundColor: "#fff",
          flex: 1
        }}
      >

        {/* AppBar */}
        <View
          style={{
            backgroundColor: "#00a46c",
            height: "25%",
            borderBottomLeftRadius: 20,
            borderBottomRightRadius: 20,
            paddingHorizontal: 20,
          }}
        >
          <Image
            source={require("../../assets/images/1.png")}
            style={{
              height: 10,
              width: 20,
              marginTop: 50,
            }}
          />
          <View style={{
              width: "100%",
              marginTop: 25,
              flexDirection: "row",
              alignItems: 'center',
            }}
          >
            <View style={{width: "50%"}}>
              <Text style={{fontSize: 28, color: "#fff", fontWeight: 'bold'}}>Hi Guest</Text>
            </View>

            <View style={{width: "50%", alignItems: "flex-end"}}>
              <Image
                source={require("../../assets/images/g.png")}
                style={{height: 60, width: 60}}
              />
            </View>
          </View>
        </View>

        {/* Search Bar */}
        <LinearGradient
          colors={["rgba(0, 164, 109, 0.4)", "transparent"]}
          style={{
            left: 0,
            right: 0,
            height: 60,
            marginTop: -25,
          }}>

          <View style={{
              backgroundColor: '#fff',
              paddingVertical: 8,
              paddingHorizontal: 20,
              marginHorizontal: 20,
              borderRadius: 15,
              marginTop: 5,
              flexDirection: 'row',

            }}>
            <TextInput
              placeholder="Search"
              placeholderTextColor="#b1e5d3"
              style={{
                fontWeight: "bold",
                fontSize: 18,
                width: 260,
              }}
            />
          <Image source={require("../../assets/images/3.png")} style={{height: 20, width: 20, position: 'absolute', top: 9, right: 20}}/>
          </View>
        </LinearGradient>

        {/* Recommend Bar */}
        <View style={{ flexDirection: 'row', paddingHorizontal: 20, width: "100%", alignItems: 'center', marginTop: -10}}>
          <View style={{ width: "50%"}}>
            <Text style={{ fontWeight:'bold', fontSize: 17, color: "#585a61"}}>Recommend</Text>
            <View style={{ height: 4, width: 105, backgroundColor: "#b1e5d3"}}></View>
          </View>
          <View style={{ width: "50%", alignItems: 'flex-end'}}>
            <View style={{ backgroundColor: "#00a46c", paddingHorizontal: 20, paddingVertical: 8, borderRadius: 15}}>
              <Text style={{ fontSize: 13, color: "#fff", fontWeight: "bold"}}>More</Text>
            </View>
          </View>
        </View>

        {/* Recommend Horizontal List */}
        <ScrollView horizontal showHorizontalScrollIndicator={false} style={{height: 280}}>
          <LinearGradient colors={["rgba(0, 164, 109, 0.09)", "transparent"]} style={{ position:'absolute', height: 90, top: 0, left: 0, right: 0, marginTop: 220 }}/>

          {/* TouchableOpacity Item */}
          <TouchableOpacity style={styles.recommendItemTouchableOpacity}>
            <Image source={require("../../assets/images/4.png")} />
            <View style={styles.recommendItemDescristionContainer}>
              <Text style={styles.recommendItemTextName}>SAMANTHA</Text>
              <Text style={styles.recommendItemTextCost}>$400</Text>
            </View>
            <Text style={styles.recommendItemTextComeFrom}>RUSSIA</Text>
          </TouchableOpacity>

          {/* TouchableOpacity Item */}
          <TouchableOpacity style={styles.recommendItemTouchableOpacity}>
            <Image source={require("../../assets/images/5.png")} />
            <View style={styles.recommendItemDescristionContainer}>
              <Text style={styles.recommendItemTextName}>ANGELICA</Text>
              <Text style={styles.recommendItemTextCost}>$600</Text>
            </View>
            <Text style={styles.recommendItemTextComeFrom}>AMERICA</Text>
          </TouchableOpacity>

          {/* TouchableOpacity Item */}
          <TouchableOpacity style={styles.recommendItemTouchableOpacity}>
            <Image source={require("../../assets/images/6.png")} />
            <View style={styles.recommendItemDescristionContainer}>
              <Text style={styles.recommendItemTextName}>ROSEMARY</Text>
              <Text style={styles.recommendItemTextCost}>$500</Text>
            </View>
            <Text style={styles.recommendItemTextComeFrom}>USA</Text>
          </TouchableOpacity>
        </ScrollView>

        {/* Feartured Bar */}
        <View style={{ flexDirection: 'row', paddingHorizontal: 20, width: "100%", alignItems: 'center'}}>
          <View style={{ width: "50%"}}>
            <Text style={{ fontWeight:'bold', fontSize: 17, color: "#585a61"}}>Featured Plants</Text>
            <View style={{ height: 4, width: 130, backgroundColor: "#b1e5d3"}}></View>
          </View>
          <View style={{ width: "50%", alignItems: 'flex-end'}}>
            <View style={{ backgroundColor: "#00a46c", paddingHorizontal: 20, paddingVertical: 8, borderRadius: 15}}>
              <Text style={{ fontSize: 13, color: "#fff", fontWeight: "bold"}}>More</Text>
            </View>
          </View>
        </View>
    </View>
  )
}

export default HomePage;

const styles = StyleSheet.create({
    recommendItemTouchableOpacity: {
      height: 250,
      elevation: 2,
      backgroundColor: "#fff",
      marginLeft: 20, marginTop: 20,
      marginBottom: 10,
      width: 160,
      borderRadius: 15
    },

    recommendItemDescristionContainer: {
      paddingTop: 10,
      paddingHorizontal: 10,
      flexDirection: 'row'
    },

    recommendItemTextName: {
      fontWeight: 'bold'
    },

    recommendItemTextCost: {
      fontWeight: "bold",
      paddingLeft: 20,
      color: "#00a46c"
    },

    recommendItemTextComeFrom: {
      paddingHorizontal: 10,
      paddingTop: 3,
      fontWeight: 'bold',
      color: '#b1e5d3'
    },
});
